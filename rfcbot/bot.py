from typing import Any
from rfcbot.rfc import Rfc
from rfcbot.config import Config


def main(conf: dict[str, Any]):
    config = Config(conf)
    bot = Rfc(config)

    status_text: str = bot.scan_repos([config.rfc_repo])
    bot.post_update(status_text)
