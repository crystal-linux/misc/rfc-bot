from gitlab import Gitlab
from gitlab.v4.objects import GroupMember, ProjectIssue, ProjectIssueAwardEmoji
from datetime import datetime
from rfcbot.config import Config
from typing import Union


class Rfc:
    """
    The class that contains the methods for the bot.


    Methods
    -------
    post_update(status_text: str)
        Posts the content of status_test to the bots status issue.
    scan_repos(scanned_repos: list[str]) -> str
        Scans the given list of repositories and finds if they have RFC issues.
        If RFC issues are found, their reactions are checked and if half of
        the members reacted thumbs up, the issue gets locked, and it returns a
        status_text string that is used by the post_update() function.
    """

    def __init__(self, config: Config):
        self.config = config
        self.gl = Gitlab(url=config.gitlab_instance, private_token=config.private_token)

    def post_update(self, status_text: str):
        """
        Post a status update to the bots status repo.

        This function goes to the first issue on the status repo,
        and it posts the status_text parameter as a comment to the issue.

        Parameters
        ----------
        status_text : str
            The string that the function posts as a comment
        """
        if status_text is not None:
            repo = self.gl.projects.get(self.config.status_repo)
            issue = repo.issues.get(1)
            issue.notes.create(
                {"body": f"{status_text}\n\nTimestamp is {datetime.now().isoformat()}"}
            )

    def scan_repos(self, scanned_repos: list[str]) -> str:
        """
        Scan the given list of repositories and finds if they have RFC issues.

        If RFC issues are found, their reactions are checked and if half of
        the members reacted thumbs up, the issue gets locked, and it returns a
        status_text string that is used by the post_update() function.

        Parameters
        ----------
        scanned_repos : list[str]
            A list of repos that should be scanned.

        Returns
        -------
        str
            A status string that should be posted to the bots status page.
        """
        status_text: Union[str, None] = ""
        for tgt in scanned_repos:
            repo = self.gl.projects.get(tgt)
            status_text += f"# Examining {repo.attributes.get('name')}\n"

            crystal_logins: list[str] = [
                x
                for x in self.get_team_members(self.config.team_groups)
                if not x in self.config.ignore_users
            ]

            rfcs_passed: int = 0

            issue_list: list[ProjectIssue] = repo.issues.list()

            for issue in issue_list:
                title: str = issue.attributes.get("title")
                if "RFC" in title:
                    print(
                        f"theres an rfc lol. its title: {issue.attributes.get('title')}"
                    )
                    reactions: list[ProjectIssueAwardEmoji] = issue.awardemojis.list()
                    if len(reactions) != 0:
                        total_votes_for: int = 0
                        total_votes_against: int = 0
                        for reaction in reactions:
                            user_name: str = reaction.attributes.get("user").get(
                                "username"
                            )
                            if user_name in crystal_logins:
                                reaction_name: str = reaction.attributes.get("name")
                                if reaction_name == "thumbsup":
                                    total_votes_for += 1
                                elif reaction_name == "thumbsdown":
                                    total_votes_against += 1
                        print(f"Total votes for the RFC: {str(total_votes_for)}")
                        print(
                            f"Total votes against the RFC: {str(total_votes_against)}"
                        )
                        if (total_votes_for / len(crystal_logins)) > 0.5:
                            print("This RFC has passed. Adding a comment")
                            if not issue.attributes.get("discussion_locked"):
                                issue.notes.create(
                                    {
                                        "body": "This resolution has enough votes to be considered passed. "
                                        "Please close the issue once appropriate actions have been taken. "
                                        "- Beep Boop (I'm a bot, and this action was performed automagically)"
                                    }
                                )
                                issue.discussion_locked = True
                                issue.save()
                                rfcs_passed += 1
                                if not issue.attributes.get("state_event") == "closed":
                                    status_text += f"* Suggested that {title} to be acted upon, as it has passed.\n"
                        else:
                            print("Not enough votes to pass")
            if rfcs_passed == 0:
                status_text = None

        return status_text

    def get_team_members(self, groups: list[str]) -> list[str]:
        """
        Scan the given groups and returns a list containing the usernames of the team members.

        Parameters
        ----------
        groups : list[str]
            A list of the groups you want to be scanned.

        Returns
        -------
        list[str]
            A list containing the usernames of the groups scanned.
        """
        crystal_logins: list[str] = []

        for group in groups:
            gl_group = self.gl.groups.get(group)
            gl_group_members: list[GroupMember] = gl_group.members_all.list(
                get_all=True
            )

            for member in gl_group_members:
                if member.attributes.get("username") not in crystal_logins:
                    crystal_logins.append(member.attributes.get("username"))

        return crystal_logins
