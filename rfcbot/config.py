from typing import Any


class Config:
    """
    The config class.

    This class contains all the options configured in config.toml

    Parameters
    ----------
    config : dict[str, Any]
        The TOML string from config.toml

    Attributes
    -----------
    gitlab_instance : str
        The URL of the Gitlab Instance.
    private_token : str
        Your bots access token.
    rfc_repo : str
        The repository to scan for RFCs on.
    status_repo : str
        The repository the bot has its status issue in.
    ignore_users : str
        A list of users to ignore when calculating RFC status
    team_groups : list[str]
        A list of groups that team members are in.
    """

    gitlab_instance: str
    private_token: str
    rfc_repo: str
    status_repo: str
    ignore_users: list[str]
    team_groups: list[str]

    def __init__(self, config: dict[str, Any]):
        if not config.get("gitlab_instance"):
            raise TypeError(
                "Can't find gitlab_instance. Maybe configure it in config.toml?"
            )
        if not config.get("private_token"):
            raise TypeError(
                "Can't find private_token. Maybe configure it in config.toml?"
            )
        if not config.get("rfc_repo"):
            raise TypeError("Can't find rfc_repo. Maybe configure it in config.toml?")
        if not config.get("status_repo"):
            raise TypeError(
                "Can't find status_repo. Maybe configure it in config.toml?"
            )
        if not config.get("ignore_users"):
            raise TypeError(
                "Can't find ignore_users. Maybe configure it in config.toml?"
            )
        if not config.get("team_groups"):
            raise TypeError(
                "Can't find team_groups. Maybe configure it in config.toml?"
            )
        self.gitlab_instance = config.get("gitlab_instance")
        self.private_token = config.get("private_token")
        self.rfc_repo = config.get("rfc_repo")
        self.status_repo = config.get("status_repo")
        self.ignore_users = config.get("ignore_users")
        self.team_groups = config.get("team_groups")
