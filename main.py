from os import path
import tomli
from rfcbot import bot

if __name__ == "__main__":
    with open(
        path.join(path.dirname(path.realpath(__file__)), "config.toml"), "rb"
    ) as f:
        config = tomli.load(f)
        bot.main(config)
