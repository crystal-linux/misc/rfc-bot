# rfc-bot-gitlab
## Install deps
You need the `poetry` package manager to install the 
python dependencies for this package.

If you're on an Arch based distro, this would be `python-poetry`.

### Run this command to install the dependencies without dev dependencies
```commandline
poetry update --without=dev
```

### Or with dev dependencies
```commandline
poetry update
```

## Running the bot
The bot requires some environment variables to run.
Copy the `.env.example` template to `.env` and edit the variables
to your specific configuration.

And to run the bot, you just need to run this command.
```commandline
poetry run python3 main.py
```

## Contributing
If you want to contribute, you should run the `black` formatter
before you make a commit since the CI enforces 
the `black` formatter

### To run the `black` formatter, run this command if you've installed the dev dependencies
```commandline
poetry run black main.py rfcbot/
```
